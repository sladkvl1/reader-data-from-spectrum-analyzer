User's Guide.

1) The "OPEN" button opens the file selection dialog box. The same function is performed by pressing the 'o' key on the keyboard. 
You need to select the file with data from the spectral analyzer ("Name.DAT").

2) The "SAVE" button saves the graph as a JPG file without any other GUI elements. 
The same function is performed by pressing the 's' key on the keyboard.

3) You can edit figure properties such as grid, limits, legend, title, font size, and ticks. 
Editing is activated by clicking on a specific checkbox. 

If you press the "tab" key, you activate the ability to toggle checkboxes using the keyboard keys. The next "tab" press will disable this capability.   
Below is a list of the active keys:

'g' - activates the checkbox "Grid".
'l' - activates the checkbox "Legend".
'x' - activates the checkbox "XLim".
'y' - activates the checkbox "YLim".
't' - activates the checkbox "Title".
'f' - activates the checkbox "Font Size".
'X' - activates the checkbox "XTicks".

You can also change the font size of the graph (if the checkbox is enabled) using the arrow keys on the keyboard:

'<-' - makes the font size smaller.
'->' - makes the font size bigger.

The names of traces in the legend can be modified by typing names separated by ';'.

Ticks can be modified by typing values separated by "space".  

4) The information about the measurement can be seen on the left panel.





Návod k použití.

1) Tlačítko "OPEN" otevírá dialogové okno výběru souboru. Stejnou funkci má klávesa 'o' na klávesnici. 
Musíte vybrat soubor s daty ze spektrálního analyzátoru ("Name.DAT").

2) Tlačítko "SAVE" ukládá graf jako soubor JPG bez jakýchkoli jiných prvků uživatelského rozhraní. 
Stejnou funkci má klávesa 's' na klávesnici.

3) Můžete upravovat vlastnosti grafu, jako jsou mřížka, limity, legenda, nadpis, velikost písma a značky. 
Úpravy se spouštějí kliknutím na určité zaškrtávací políčko.

Pokud stisknete klávesu Tab, aktivujete možnost přepínání zaškrtávacích polí pomocí klávesnice. Další stisknutí klávesy Tab tuto schopnost deaktivuje.
Zde je seznam aktivních kláves:

'g' - aktivuje zaškrtávací políčko "Grid".
'l' - aktivuje zaškrtávací políčko "Legend".
'x' - aktivuje zaškrtávací políčko "XLim".
'y' - aktivuje zaškrtávací políčko "YLim".
't' - aktivuje zaškrtávací políčko "Title".
'f' - aktivuje zaškrtávací políčko "Font Size".
'X' - aktivuje zaškrtávací políčko "XTicks".

Můžete také měnit velikost písma grafu (pokud je zaškrtávací políčko aktivní) pomocí šipek na klávesnici:

'<-' - zmenšuje velikost písma.
'->' - zvětšuje velikost písma.

Jména pruběhů ve legendě lze upravit zadáním jmen oddělených ';'.

XLicks mohou být upraveny zadáním hodnot oddělených mezerou.

4) Informace o měření můžete vidět na levém panelu.


