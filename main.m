function main()
%% Analyzer
%% Start parameters
% Size of GUI
figsize =           [1230 650];
screenSize = get(groot,'ScreenSize');

% Sizes and positions of Static GUI elements
figPosition =       [(screenSize(3:4) - figsize)/2, figsize];
AxPos =             [290, 20, 800, 600];
ButtPosOpen =       [1100, 570, 110, 40];
ButtPosSave =       [1100, 520, 110, 40];
CheckPosGrid =      [1100, 490, 50, 20];
CheckPosLegend =    [1100, 460, 80, 20];
EditPosLegend =     [1120, 430, 100, 20];
CheckPosXlim =      [1100, 400, 60, 20];
EditPosXmin =       [1180, 370, 40, 20];
labelXmin =         [1100, 370, 80, 20];
EditPosXmax =       [1180, 340, 40, 20];
labelXmax =         [1100, 340, 90, 20];
CheckPosYlim =      [1100, 310, 60, 20];
EditPosYmin =       [1180, 280, 40, 20];
labelYmin =         [1100, 280, 80, 20];
EditPosYmax =       [1180, 250, 40, 20];
labelYmax =         [1100, 250, 90, 20];
CheckPosTitle =     [1100, 220, 60, 20];
EditPosTitle =      [1120, 190, 100 ,20];
CheckPosFontSize =  [1100, 160, 100, 20];
SliderPosFontSize = [1115, 130, 100, 3];
CheckPosXTick =     [1100, 60, 120, 20];
EditPosXTick =      [1120, 30, 100, 20];
PanelPos =          [10, 35, 250, 580];

% Start variables's values
startPosLabels = 510;         % Position of the first info label
offsetLabels = 40;            % Distance between labels
FontSizeLabels = 18;          % Font Size of labels
FontSizeFig = 20;             % Font Size of Graph
FontSizeButtons = 20;         % Font Size of Buttons
FontSizeElements = 15;        % Font Size of interactive GUI elements
full_path = [];               % File full path
traces = [];                  % Names of Traces
Values = 0;                   % Number of amplitude/frequency values
info = {};                    % Base measurement information
xlim_min = 0.15;              % Default XLim minimum
xlim_max = 30;                % Default XLim maximum
ylim_min = 0;                 % Default YLim minimum
ylim_max = 80;                % Default YLim maximum
pattern = ["Type","Version","Date","Mode","RBW","Meas Time",...
    "Auto Ranging","RF Att","Auto Preamp","Preamp"];    % info parametrs

%% Static GUI elements
hFig = uifigure('Position', figPosition, ...
    'Name', 'Painting');
hAx = uiaxes(hFig,'Position', AxPos, ...
    'Interactions',[],'Toolbar',[]);
hLegend = legend(hAx);
hLegend.Visible = "off";
hButtOpen = uibutton(hFig,"Text","Open", ...
    'Position', ButtPosOpen, ...
    'FontSize', FontSizeButtons);
hButtSave = uibutton(hFig,"Text","Save", ...
    'Position', ButtPosSave, ...
    'FontSize', FontSizeButtons);
hCheckGrid = uicheckbox(hFig, 'Text', 'Grid', ...
    'Position', CheckPosGrid,'FontSize', FontSizeElements);
hCheckLegend = uicheckbox(hFig, 'Text', 'Legend:', ...
    'Position', CheckPosLegend,'FontSize', FontSizeElements);
hEditLegend = uieditfield(hFig, 'text', ...
    'Position', EditPosLegend, ...
    'Enable','off','FontSize', FontSizeElements);
hCheckXlim = uicheckbox(hFig, 'Text', 'XLim:', ...
    'Position', CheckPosXlim,'FontSize', FontSizeElements);
hEditXmin = uieditfield(hFig, 'numeric', ...
    'Position', EditPosXmin, 'Value', xlim_min, ...
    'Enable','off','FontSize', FontSizeElements);
hEditXmax = uieditfield(hFig, 'numeric', ...
    'Position', EditPosXmax, 'Value', xlim_max, ...
    'Enable','off','FontSize', FontSizeElements);
uilabel(hFig, "Text","Min (MHz):", 'Position', labelXmin, ...
    'FontSize', FontSizeElements);
uilabel(hFig, "Text","Max (MHz):", 'Position', labelXmax, ...
    'FontSize', FontSizeElements);
hCheckYlim = uicheckbox(hFig, 'Text', 'YLim:', ...
    'Position', CheckPosYlim,'FontSize', FontSizeElements);
hEditYmin = uieditfield(hFig, 'numeric', ...
    'Position', EditPosYmin, 'Value', ylim_min, ...
    'Enable','off','FontSize', FontSizeElements);
hEditYmax = uieditfield(hFig, 'numeric', ...
    'Position', EditPosYmax, 'Value', ylim_max, ...
    'Enable','off','FontSize', FontSizeElements);
uilabel(hFig, "Text","Min (dB):", 'Position', labelYmin, ...
    'FontSize', FontSizeElements);
uilabel(hFig, "Text","Max (dB):", 'Position', labelYmax, ...
    'FontSize', FontSizeElements);
hCheckTitle = uicheckbox(hFig, 'Text', 'Title:', ...
    'Position', CheckPosTitle,'FontSize', FontSizeElements);
hEditTitle = uieditfield(hFig, 'text', ...
    'Position', EditPosTitle, "Value","New Title", ...
    'Enable','off','FontSize', FontSizeElements);
hCheckFontSize = uicheckbox(hFig, 'Text', 'Font Size:', ...
    'Position', CheckPosFontSize,'FontSize', FontSizeElements);
hSliderFontSize = uislider(hFig,'Limits', [0, 30],'Enable','off', ...
    'Position', SliderPosFontSize, 'Orientation', 'horizontal', ...
    'MajorTicks', 0:5:30,'Value', FontSizeFig);
hCheckXTick = uicheckbox(hFig, 'Text', 'XTicks (MHz):', ...
    'Position', CheckPosXTick,'FontSize', FontSizeElements);
hEditXTick = uieditfield(hFig, "text", ...
    'Position', EditPosXTick, "Value", "0.15 1 10 30", ...
    'Enable','off','FontSize', FontSizeElements);
hPanel = uipanel(hFig,"Title","Information", ...
    "FontSize", 20, "BackgroundColor","white", ...
    "Position", PanelPos);
%% Definition Callback functions
hButtOpen.ButtonPushedFcn = @(src, event) OpenFile();
hButtSave.ButtonPushedFcn = @(src, event) SavePlot();
hCheckGrid.ValueChangedFcn = @(src, event) GridOn();
hCheckLegend.ValueChangedFcn = @(src, event) LegendOn();
hEditLegend.ValueChangedFcn = @(src, event) ChangeLegend(src);
hCheckXlim.ValueChangedFcn = @(src, event) ChangeXlim();
hEditXmin.ValueChangedFcn = @(src, event) ChangeXmin(src);
hEditXmax.ValueChangedFcn = @(src, event) ChangeXmax(src);
hCheckYlim.ValueChangedFcn = @(src, event) ChangeYlim();
hEditYmin.ValueChangedFcn = @(src, event) ChangeYmin(src);
hEditYmax.ValueChangedFcn = @(src, event) ChangeYmax(src);
hCheckTitle.ValueChangedFcn = @(src, event) TitleOn();
hEditTitle.ValueChangedFcn = @(src, event) ChangeTitle(src);
hCheckFontSize.ValueChangedFcn = @(src, event) FontSizeOn();
hSliderFontSize.ValueChangingFcn = @(src, event) ChangeFontSize(event);
hCheckXTick.ValueChangedFcn = @(src, event) XTickOn();
hEditXTick.ValueChangedFcn = @(src, event) ChangeXTick(src);
hFig.WindowKeyPressFcn = @(src, event) TabPressed(event);
%% Callback functions
    function OpenFile()

        % File selection
        [file, path] = uigetfile('*.*', 'Select a file');
        if isequal(file, 0)
            disp('No file selected');
            return;
        end

        % Reset values
        traces = [];
        info = {};
        labels = findall(hPanel, 'Type', 'uilabel');
        delete(labels);

        % Reading information about a file
        full_path = fullfile(path, file);
        fileID = fopen(full_path, 'r');
        if fileID == -1                    % checking file opening
            disp('The file could not be opened.');
            return;
        end
        tline = fgetl(fileID);             % Read line from file
        while ischar(tline)
            match = regexp(tline, 'Values;(\d+);', 'tokens');
            if contains(tline,'TRACE')
                traces{end+1} = tline;
            end
            if contains(tline,pattern)
                info{end+1} = tline;
            end
            if ~isempty(match)
                Values = str2double(match{1}{1});
            end
            tline = fgetl(fileID);
        end
        fclose(fileID);

        % Writing Data
        fileID = fopen(full_path, 'r');
        tline = fgetl(fileID);
        frequencies = NaN(Values,length(traces));
        amplitudes = NaN(Values,length(traces));
        tmp = [0 0];
        while ischar(tline)
            match = regexp(tline, '(\d+\.\d+);(\d+\.\d+);', 'tokens');
            if contains(tline,'TRACE')
                tmp(2) = tmp(2) + 1;
                tmp(1) = 0;
            end
            if ~isempty(match)
                tmp(1) = tmp(1) + 1;
                frequencies(tmp(1),tmp(2)) = str2double(match{1}{1});
                amplitudes(tmp(1),tmp(2)) = str2double(match{1}{2});
            end
            tline = fgetl(fileID);
        end
        fclose(fileID);

        % Matrixes format correction
        frequencies(:, any(isnan(frequencies))) = [];
        amplitudes(:, any(isnan(amplitudes))) = [];
        traces = strrep(traces, ':', '');
        traces = traces(1:size(amplitudes,2));
        traces = string(traces);
        for x = 1:length(info)
            info{x} = regexprep(info{x},';',': ',1);
        end
        info = cellfun(@(str) strrep(str, ';', ' '), ...
            info, 'UniformOutput', false);

        % Adding elements to uiaxes
        semilogx(hAx,frequencies,amplitudes);
        hAx.XLim = (10^6).*[xlim_min xlim_max];
        hAx.YLim = [ylim_min ylim_max];
        hAx.XGrid = hCheckGrid.Value;
        hAx.YGrid = hCheckGrid.Value;
        hAx.XLabel.String = 'Frequency [MHz]';
        hAx.YLabel.String = 'Interference voltage level [dB/uV]';
        hLegend.Visible = hCheckLegend.Value;
        hLegend.String = traces;
        Traces_str = join(traces, ';');
        hEditLegend.Value = Traces_str;
        hLegend.Interruptible = "off";
        hLegend.Location = "best";
        hAx.FontSize = hSliderFontSize.Value;
        hEditXTick.Value = string(hEditXTick.Value);
        Ticks = str2double(split(hEditXTick.Value,' '));
        hAx.XTick = (10^6).*Ticks;
        hAx.XTickLabel = num2cell(Ticks);
        hAx.Title.String = hEditTitle.Value;
        hAx.Title.Visible = hCheckTitle.Value;

        % Adding measurement information
        uilabel(hPanel, "Text",info(contains(info, 'Type')), ...
            'Position', [10, startPosLabels, 200, 30], ...
            'FontSize', FontSizeLabels);
        uilabel(hPanel, "Text",info(contains(info, 'Date')), ...
            'Position', [10, startPosLabels-offsetLabels, 200, 30], ...
            'FontSize', FontSizeLabels);
        uilabel(hPanel, "Text",info(contains(info, 'RBW')), ...
            'Position', [10, startPosLabels-2*offsetLabels, 200, 30], ...
            'FontSize', FontSizeLabels);
        uilabel(hPanel, "Text",info(contains(info, 'Meas Time')), ...
            'Position', [10, startPosLabels-3*offsetLabels, 200, 30], ...
            'FontSize', FontSizeLabels);
        uilabel(hPanel, "Text",info(contains(info, 'RF Att')), ...
            'Position', [10, startPosLabels-4*offsetLabels, 200, 30], ...
            'FontSize', FontSizeLabels);
        uilabel(hPanel, "Text",info(contains(info, 'Auto Preamp')), ...
            'Position', [10, startPosLabels-5*offsetLabels, 200, 30], ...
            'FontSize', FontSizeLabels);
        uilabel(hPanel, "Text","Number of traces: ", ...
            'Position', [10, startPosLabels-6*offsetLabels, 150, 30], ...
            'FontSize', FontSizeLabels);
        uilabel(hPanel, "Text",num2str(size(amplitudes,2)), ...
            'Position', [160, startPosLabels-6*offsetLabels, 10, 30], ...
            'FontSize', FontSizeLabels);
        uilabel(hPanel, "Text","Number of sampels: ", ...
            'Position', [10, startPosLabels-7*offsetLabels, 170, 30], ...
            'FontSize', FontSizeLabels);
        uilabel(hPanel, "Text",num2str(Values), ...
            'Position', [178, startPosLabels-7*offsetLabels, 80, 30], ...
            'FontSize', FontSizeLabels);
        Trace_Mode = info(contains(info, 'Trace Mode'));
        for x = 1:length(traces)
            uilabel(hPanel, "Text",Trace_Mode{x}, 'Position', ...
                [10, startPosLabels-offsetLabels.*(x+7), 250, 30], ...
                'FontSize', FontSizeLabels);
        end
    end

    function SavePlot()
        exportgraphics(hFig, 'Figure.jpg', 'Resolution', 400);
    end

    function GridOn()
        if hCheckGrid.Value
            hAx.XGrid = 'on';
            hAx.YGrid = 'on';
        else
            hAx.XGrid = 'off';
            hAx.YGrid = 'off';
        end
    end

    function LegendOn()
        if hCheckLegend.Value
            hLegend.Visible = "on";
            hEditLegend.Enable = "on";
            hLegend.String = split(string(hEditLegend.Value),';');
        else
            hLegend.Visible = "off";
            hEditLegend.Enable = "off";
        end
    end

    function ChangeLegend(src)
        hLegend.String = split(string(src.Value),';');
    end

    function ChangeXlim()
        if hCheckXlim.Value
            hEditXmin.Enable = 'on';
            hEditXmax.Enable = 'on';
            xlim_min = hEditXmin.Value;
            xlim_max = hEditXmax.Value;
        else
            hEditXmin.Enable = 'off';
            hEditXmax.Enable = 'off';
            xlim_min = 0.15;
            xlim_max = 30;
        end
        hAx.XLim = (10^6).*[xlim_min xlim_max];
    end

    function ChangeXmin(src)
        if src.Value < xlim_max
            xlim_min = src.Value;
            hAx.XLim = (10^6).*[xlim_min xlim_max];
        end
    end

    function ChangeXmax(src)
        if src.Value > xlim_min
            xlim_max = src.Value;
            hAx.XLim = (10^6).*[xlim_min xlim_max];
        end
    end

    function ChangeYlim()
        if hCheckYlim.Value
            hEditYmin.Enable = 'on';
            hEditYmax.Enable = 'on';
            ylim_min = hEditYmin.Value;
            ylim_max = hEditYmax.Value;
        else
            hEditYmin.Enable = 'off';
            hEditYmax.Enable = 'off';
            ylim_min = 0;
            ylim_max = 80;
        end
        hAx.YLim = [ylim_min ylim_max];
    end

    function ChangeYmin(src)
        if src.Value < ylim_max
            ylim_min = src.Value;
            hAx.YLim = [ylim_min ylim_max];
        end
    end

    function ChangeYmax(src)
        if src.Value > ylim_min
            ylim_max = src.Value;
            hAx.YLim = [ylim_min ylim_max];
        end
    end

    function TitleOn()
        if hCheckTitle.Value
            hAx.Title.Visible = "on";
            hEditTitle.Enable = "on";
        else
            hAx.Title.Visible = "off";
            hEditTitle.Enable = "off";
        end
    end

    function ChangeTitle(src)
        hAx.Title.String = src.Value;
    end

    function FontSizeOn()
        if hCheckFontSize.Value
            hSliderFontSize.Enable = "on";
            hAx.FontSize = hSliderFontSize.Value;
        else
            hSliderFontSize.Enable = "off";
            hAx.FontSize = FontSizeFig;
        end
    end

    function ChangeFontSize(event)
        if event.Value > 0
            hAx.FontSize = event.Value;
        end
    end

    function XTickOn()
        if hCheckXTick.Value
            hEditXTick.Enable = "on";
            hEditXTick.Value = string(hEditXTick.Value);
            Ticks = str2double(split(hEditXTick.Value,' '));
            hAx.XTick = (10^6).*Ticks;
            hAx.XTickLabel = num2cell(Ticks);
        else
            hEditXTick.Enable = "off";
            hAx.XTick = (10^6).*[0.15 1 10 30];
            hAx.XTickLabel = {'0.15','1','10','30'};
        end
    end

    function ChangeXTick(src)
        Ticks = str2double(split(string(src.Value),' '));
        hAx.XTick = (10^6).*Ticks;
        hAx.XTickLabel = num2cell(Ticks);
    end

    function TabPressed(event)
        if event.Key == "tab"
            hFig.WindowKeyPressFcn = @(src, event) KeyPressed(event);
        end
    end

    function KeyPressed(event)
        step = 1;
        if event.Key == "tab"
            hFig.WindowKeyPressFcn = @(src, event) TabPressed(event);
        end
        if hCheckFontSize.Value
            switch event.Key
                case 'leftarrow'
                    if hSliderFontSize.Value - step >= min(hSliderFontSize.Limits)
                        hSliderFontSize.Value = hSliderFontSize.Value - step;
                    end
                    ChangeFontSize(hSliderFontSize)
                case 'rightarrow'
                    if hSliderFontSize.Value + step <= max(hSliderFontSize.Limits)
                        hSliderFontSize.Value = hSliderFontSize.Value + step;
                    end
                    ChangeFontSize(hSliderFontSize)
            end
        end
        switch event.Character
            case 'g'
                hCheckGrid.Value = ~hCheckGrid.Value;
                GridOn();
            case 'l'
                hCheckLegend.Value = ~hCheckLegend.Value;
                LegendOn();
            case 'x'
                hCheckXlim.Value = ~hCheckXlim.Value;
                ChangeXlim();
            case 'y'
                hCheckYlim.Value = ~hCheckYlim.Value;
                ChangeYlim();
            case 't'
                hCheckTitle.Value = ~hCheckTitle.Value;
                TitleOn();
            case 'f'
                hCheckFontSize.Value = ~hCheckFontSize.Value;
                FontSizeOn();
            case 'X'          
                hCheckXTick.Value = ~hCheckXTick.Value;
                XTickOn();
            case 'o'
                OpenFile();
            case 's'
                SavePlot();
        end
    end

end